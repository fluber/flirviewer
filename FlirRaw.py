# import the necessary packages
import os, re
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import argparse

if __name__ == "__main__":
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--Path", required=True,
    	help="root path of the file raw file")
    ap.add_argument("-o", "--OutputPath", required=True,
    	help="output path of the csv file")
    ap.add_argument("-sd", "--StartDate", required=False, default='*',
    	help="start date of the file raw file, YYYYMMDD or *")
    ap.add_argument("-ed", "--EndDate", required=False, default='*',
    	help="end date of the file raw file, YYYYMMDD or *")
    ap.add_argument("-sc", "--StartCell", required=False, default='*',
    	help="start cell of the file raw file, RRCC or *")
    ap.add_argument("-ec", "--EndCell", required=False, default='*',
    	help="end cell of the file raw file, RRCC or *")
    ap.add_argument("-min", "--MinTemperature", required=False, default='*',
    	help="min temperature of the file raw file, min or *")
    ap.add_argument("-max", "--MaxTemperature", required=False, default='*',
    	help="max temperature of the file raw file, max or *")
    args = vars(ap.parse_args())
    if '*' == args['StartDate']:
        start_date = datetime.strptime('20000101', '%Y%m%d')
    else:
        start_date = datetime.strptime(args['StartDate'], '%Y%m%d')

    if '*' == args['EndDate']:
        end_date = datetime.now()
    else:
        end_date = datetime.strptime(args['EndDate'], '%Y%m%d')

    if '*' == args['StartCell']:
        start_cell = '0101'
    else:
        start_cell = args['StartCell']

    if '*' == args['EndCell']:
        end_cell = '0918'
    else:
        end_cell = args['EndCell']
    rootdir = args['Path']
    output_dir = args['OutputPath']

    isExist = os.path.exists(output_dir)
    if not isExist:
       # Create a new directory because it does not exist
       os.makedirs(output_dir)

    regex = re.compile(r"(\d{8})_\d{6}_\d_(\d{4}).*raw$")
    for root, dirs, files in os.walk(rootdir):
        for file in files:
            m = regex.match(file)
            if m:
                file_date = datetime.strptime(m.groups()[0], "%Y%m%d")
                file_cell = m.groups()[1]
                if file_date >= start_date and file_date <= end_date:
                    if file_cell >= start_cell and file_cell <= end_cell:
                        file_name = os.path.join(root, file)
                        if ((os.path.getsize(file_name) / 1000) < 1024):
                            with open(file_name, 'rb') as rawing:
                                data = np.fromfile(rawing, np.uint16)
                                raw_data = np.reshape(data, (480, 640))
                                temperature_data = raw_data * 0.01 - 273.15
                                if '*' != args['MinTemperature']:
                                    temperature_data[temperature_data < float(args['MinTemperature'])] = 0
                                if '*' != args['MaxTemperature']:
                                    temperature_data[temperature_data > float(args['MaxTemperature'])] = 0
                                output_file = os.path.join(output_dir, os.path.basename(file).split('.')[0]) + '.csv'
                                output_histogram_file = os.path.join(output_dir, os.path.basename(file).split('.')[0]) + '.png'

                                np.savetxt(output_file, temperature_data, delimiter=',', fmt='%.2f')

                                plt.title("Temperature Histogram")
                                plt.hist(temperature_data.flatten(),color='blue')
                                plt.savefig(output_histogram_file)