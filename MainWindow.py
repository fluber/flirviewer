# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QDateEdit, QFrame,
    QGridLayout, QHBoxLayout, QHeaderView, QLabel,
    QLineEdit, QMainWindow, QMenuBar, QPushButton,
    QSizePolicy, QSlider, QStatusBar, QTableWidget,
    QTableWidgetItem, QVBoxLayout, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1469, 1021)
        font = QFont()
        font.setPointSize(18)
        MainWindow.setFont(font)
        MainWindow.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.frame_2 = QFrame(self.centralwidget)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setMaximumSize(QSize(500, 16777215))
        self.frame_2.setStyleSheet(u"QFrame#frame_2 {\n"
"border: 3px solid #87CEFA;\n"
"border-radius: 20px;\n"
"}")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame_2)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label_4 = QLabel(self.frame_2)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_3.addWidget(self.label_4)

        self.edtFilrPath = QLineEdit(self.frame_2)
        self.edtFilrPath.setObjectName(u"edtFilrPath")
        self.edtFilrPath.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_3.addWidget(self.edtFilrPath)

        self.btnPath = QPushButton(self.frame_2)
        self.btnPath.setObjectName(u"btnPath")
        self.btnPath.setMinimumSize(QSize(30, 30))
        self.btnPath.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_3.addWidget(self.btnPath)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_5 = QLabel(self.frame_2)
        self.label_5.setObjectName(u"label_5")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)

        self.horizontalLayout.addWidget(self.label_5)

        self.edtStartDate = QDateEdit(self.frame_2)
        self.edtStartDate.setObjectName(u"edtStartDate")
        self.edtStartDate.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout.addWidget(self.edtStartDate)


        self.horizontalLayout_10.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_6 = QLabel(self.frame_2)
        self.label_6.setObjectName(u"label_6")
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)

        self.horizontalLayout_2.addWidget(self.label_6)

        self.edtEndDate = QDateEdit(self.frame_2)
        self.edtEndDate.setObjectName(u"edtEndDate")
        self.edtEndDate.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_2.addWidget(self.edtEndDate)


        self.horizontalLayout_10.addLayout(self.horizontalLayout_2)


        self.verticalLayout.addLayout(self.horizontalLayout_10)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.label_2 = QLabel(self.frame_2)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_6.addWidget(self.label_2)

        self.cbStartRow = QComboBox(self.frame_2)
        self.cbStartRow.addItem("")
        self.cbStartRow.addItem("")
        self.cbStartRow.addItem("")
        self.cbStartRow.addItem("")
        self.cbStartRow.addItem("")
        self.cbStartRow.addItem("")
        self.cbStartRow.setObjectName(u"cbStartRow")
        self.cbStartRow.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_6.addWidget(self.cbStartRow)

        self.cbStartCol = QComboBox(self.frame_2)
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.addItem("")
        self.cbStartCol.setObjectName(u"cbStartCol")
        self.cbStartCol.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_6.addWidget(self.cbStartCol)


        self.horizontalLayout_8.addLayout(self.horizontalLayout_6)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_3 = QLabel(self.frame_2)
        self.label_3.setObjectName(u"label_3")

        self.horizontalLayout_7.addWidget(self.label_3)

        self.cbEndRow = QComboBox(self.frame_2)
        self.cbEndRow.addItem("")
        self.cbEndRow.addItem("")
        self.cbEndRow.addItem("")
        self.cbEndRow.addItem("")
        self.cbEndRow.addItem("")
        self.cbEndRow.addItem("")
        self.cbEndRow.setObjectName(u"cbEndRow")
        self.cbEndRow.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_7.addWidget(self.cbEndRow)

        self.cbEndCol = QComboBox(self.frame_2)
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.addItem("")
        self.cbEndCol.setObjectName(u"cbEndCol")
        self.cbEndCol.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_7.addWidget(self.cbEndCol)


        self.horizontalLayout_8.addLayout(self.horizontalLayout_7)


        self.horizontalLayout_9.addLayout(self.horizontalLayout_8)

        self.btnSearch = QPushButton(self.frame_2)
        self.btnSearch.setObjectName(u"btnSearch")
        self.btnSearch.setMaximumSize(QSize(40, 40))
        self.btnSearch.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")
        icon = QIcon()
        icon.addFile(u"images/find.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnSearch.setIcon(icon)
        self.btnSearch.setIconSize(QSize(64, 64))

        self.horizontalLayout_9.addWidget(self.btnSearch)


        self.verticalLayout.addLayout(self.horizontalLayout_9)

        self.tableFlirFiles = QTableWidget(self.frame_2)
        if (self.tableFlirFiles.columnCount() < 5):
            self.tableFlirFiles.setColumnCount(5)
        __qtablewidgetitem = QTableWidgetItem()
        self.tableFlirFiles.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tableFlirFiles.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tableFlirFiles.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.tableFlirFiles.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.tableFlirFiles.setHorizontalHeaderItem(4, __qtablewidgetitem4)
        self.tableFlirFiles.setObjectName(u"tableFlirFiles")
        self.tableFlirFiles.setStyleSheet(u"")
        self.tableFlirFiles.horizontalHeader().setMinimumSectionSize(30)

        self.verticalLayout.addWidget(self.tableFlirFiles)

        self.btnLoad2 = QPushButton(self.frame_2)
        self.btnLoad2.setObjectName(u"btnLoad2")
        self.btnLoad2.setMinimumSize(QSize(0, 40))
        self.btnLoad2.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.verticalLayout.addWidget(self.btnLoad2)


        self.gridLayout.addWidget(self.frame_2, 0, 0, 2, 1)

        self.frame_3 = QFrame(self.centralwidget)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setStyleSheet(u"QFrame {\n"
"border: 3px solid #87CEFA;\n"
"border-radius: 20px;\n"
"}")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)

        self.gridLayout.addWidget(self.frame_3, 1, 1, 1, 1)

        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setMinimumSize(QSize(0, 150))
        self.frame.setMaximumSize(QSize(16777215, 150))
        self.frame.setStyleSheet(u"QFrame#frame {\n"
"border: 3px solid #87CEFA;\n"
"border-radius: 20px;\n"
"}")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_4 = QGridLayout(self.frame)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")
        self.label.setFrameShape(QFrame.NoFrame)

        self.horizontalLayout_5.addWidget(self.label)

        self.cbColorMap = QComboBox(self.frame)
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.addItem("")
        self.cbColorMap.setObjectName(u"cbColorMap")
        self.cbColorMap.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_5.addWidget(self.cbColorMap)


        self.gridLayout_4.addLayout(self.horizontalLayout_5, 0, 0, 1, 1)

        self.btnLoad = QPushButton(self.frame)
        self.btnLoad.setObjectName(u"btnLoad")
        self.btnLoad.setMinimumSize(QSize(0, 40))
        self.btnLoad.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.gridLayout_4.addWidget(self.btnLoad, 0, 1, 1, 1)

        self.btnHistogram = QPushButton(self.frame)
        self.btnHistogram.setObjectName(u"btnHistogram")
        self.btnHistogram.setMinimumSize(QSize(0, 40))
        self.btnHistogram.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.gridLayout_4.addWidget(self.btnHistogram, 0, 2, 1, 1)

        self.btnCSV = QPushButton(self.frame)
        self.btnCSV.setObjectName(u"btnCSV")
        self.btnCSV.setMinimumSize(QSize(0, 40))
        self.btnCSV.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.gridLayout_4.addWidget(self.btnCSV, 0, 3, 1, 1)

        self.btnExit = QPushButton(self.frame)
        self.btnExit.setObjectName(u"btnExit")
        self.btnExit.setMinimumSize(QSize(0, 40))
        self.btnExit.setAutoFillBackground(False)
        self.btnExit.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;\n"
"\n"
"")
        self.btnExit.setFlat(False)

        self.gridLayout_4.addWidget(self.btnExit, 0, 4, 1, 1)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.lblMinTemperature = QLabel(self.frame)
        self.lblMinTemperature.setObjectName(u"lblMinTemperature")

        self.horizontalLayout_4.addWidget(self.lblMinTemperature)

        self.hsMinTemperature = QSlider(self.frame)
        self.hsMinTemperature.setObjectName(u"hsMinTemperature")
        self.hsMinTemperature.setOrientation(Qt.Horizontal)

        self.horizontalLayout_4.addWidget(self.hsMinTemperature)

        self.lblMaxTemperature = QLabel(self.frame)
        self.lblMaxTemperature.setObjectName(u"lblMaxTemperature")

        self.horizontalLayout_4.addWidget(self.lblMaxTemperature)

        self.hsMaxTemperature = QSlider(self.frame)
        self.hsMaxTemperature.setObjectName(u"hsMaxTemperature")
        self.hsMaxTemperature.setOrientation(Qt.Horizontal)

        self.horizontalLayout_4.addWidget(self.hsMaxTemperature)

        self.btnReset = QPushButton(self.frame)
        self.btnReset.setObjectName(u"btnReset")
        self.btnReset.setMinimumSize(QSize(50, 40))
        self.btnReset.setStyleSheet(u"background-color: #87CEFA;\n"
"border-style: outset;\n"
"border-width: 2px;\n"
"border-radius: 15px;\n"
"border-color: black;\n"
"padding: 4px;")

        self.horizontalLayout_4.addWidget(self.btnReset)


        self.gridLayout_4.addLayout(self.horizontalLayout_4, 1, 0, 1, 5)


        self.gridLayout.addWidget(self.frame, 0, 1, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1469, 22))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"FlirView", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Flir Path", None))
        self.btnPath.setText(QCoreApplication.translate("MainWindow", u"...", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Start Date :", None))
        self.edtStartDate.setDisplayFormat(QCoreApplication.translate("MainWindow", u"yyyy/MM/dd", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"End Date :", None))
        self.edtEndDate.setDisplayFormat(QCoreApplication.translate("MainWindow", u"yyyy/MM/dd", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Start Cell:", None))
        self.cbStartRow.setItemText(0, QCoreApplication.translate("MainWindow", u"1", None))
        self.cbStartRow.setItemText(1, QCoreApplication.translate("MainWindow", u"2", None))
        self.cbStartRow.setItemText(2, QCoreApplication.translate("MainWindow", u"3", None))
        self.cbStartRow.setItemText(3, QCoreApplication.translate("MainWindow", u"4", None))
        self.cbStartRow.setItemText(4, QCoreApplication.translate("MainWindow", u"5", None))
        self.cbStartRow.setItemText(5, QCoreApplication.translate("MainWindow", u"6", None))

        self.cbStartCol.setItemText(0, QCoreApplication.translate("MainWindow", u"1", None))
        self.cbStartCol.setItemText(1, QCoreApplication.translate("MainWindow", u"2", None))
        self.cbStartCol.setItemText(2, QCoreApplication.translate("MainWindow", u"3", None))
        self.cbStartCol.setItemText(3, QCoreApplication.translate("MainWindow", u"4", None))
        self.cbStartCol.setItemText(4, QCoreApplication.translate("MainWindow", u"5", None))
        self.cbStartCol.setItemText(5, QCoreApplication.translate("MainWindow", u"6", None))
        self.cbStartCol.setItemText(6, QCoreApplication.translate("MainWindow", u"7", None))
        self.cbStartCol.setItemText(7, QCoreApplication.translate("MainWindow", u"8", None))
        self.cbStartCol.setItemText(8, QCoreApplication.translate("MainWindow", u"9", None))
        self.cbStartCol.setItemText(9, QCoreApplication.translate("MainWindow", u"10", None))
        self.cbStartCol.setItemText(10, QCoreApplication.translate("MainWindow", u"11", None))
        self.cbStartCol.setItemText(11, QCoreApplication.translate("MainWindow", u"12", None))
        self.cbStartCol.setItemText(12, QCoreApplication.translate("MainWindow", u"13", None))
        self.cbStartCol.setItemText(13, QCoreApplication.translate("MainWindow", u"14", None))
        self.cbStartCol.setItemText(14, QCoreApplication.translate("MainWindow", u"15", None))
        self.cbStartCol.setItemText(15, QCoreApplication.translate("MainWindow", u"16", None))
        self.cbStartCol.setItemText(16, QCoreApplication.translate("MainWindow", u"17", None))
        self.cbStartCol.setItemText(17, QCoreApplication.translate("MainWindow", u"18", None))

        self.label_3.setText(QCoreApplication.translate("MainWindow", u"End Cell:", None))
        self.cbEndRow.setItemText(0, QCoreApplication.translate("MainWindow", u"1", None))
        self.cbEndRow.setItemText(1, QCoreApplication.translate("MainWindow", u"2", None))
        self.cbEndRow.setItemText(2, QCoreApplication.translate("MainWindow", u"3", None))
        self.cbEndRow.setItemText(3, QCoreApplication.translate("MainWindow", u"4", None))
        self.cbEndRow.setItemText(4, QCoreApplication.translate("MainWindow", u"5", None))
        self.cbEndRow.setItemText(5, QCoreApplication.translate("MainWindow", u"6", None))

        self.cbEndCol.setItemText(0, QCoreApplication.translate("MainWindow", u"1", None))
        self.cbEndCol.setItemText(1, QCoreApplication.translate("MainWindow", u"2", None))
        self.cbEndCol.setItemText(2, QCoreApplication.translate("MainWindow", u"3", None))
        self.cbEndCol.setItemText(3, QCoreApplication.translate("MainWindow", u"4", None))
        self.cbEndCol.setItemText(4, QCoreApplication.translate("MainWindow", u"5", None))
        self.cbEndCol.setItemText(5, QCoreApplication.translate("MainWindow", u"6", None))
        self.cbEndCol.setItemText(6, QCoreApplication.translate("MainWindow", u"7", None))
        self.cbEndCol.setItemText(7, QCoreApplication.translate("MainWindow", u"8", None))
        self.cbEndCol.setItemText(8, QCoreApplication.translate("MainWindow", u"9", None))
        self.cbEndCol.setItemText(9, QCoreApplication.translate("MainWindow", u"10", None))
        self.cbEndCol.setItemText(10, QCoreApplication.translate("MainWindow", u"11", None))
        self.cbEndCol.setItemText(11, QCoreApplication.translate("MainWindow", u"12", None))
        self.cbEndCol.setItemText(12, QCoreApplication.translate("MainWindow", u"13", None))
        self.cbEndCol.setItemText(13, QCoreApplication.translate("MainWindow", u"14", None))
        self.cbEndCol.setItemText(14, QCoreApplication.translate("MainWindow", u"15", None))
        self.cbEndCol.setItemText(15, QCoreApplication.translate("MainWindow", u"16", None))
        self.cbEndCol.setItemText(16, QCoreApplication.translate("MainWindow", u"17", None))
        self.cbEndCol.setItemText(17, QCoreApplication.translate("MainWindow", u"18", None))

        self.btnSearch.setText("")
        ___qtablewidgetitem = self.tableFlirFiles.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("MainWindow", u"\u65e5\u671f", None));
        ___qtablewidgetitem1 = self.tableFlirFiles.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("MainWindow", u"\u5217", None));
        ___qtablewidgetitem2 = self.tableFlirFiles.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("MainWindow", u"\u6b04", None));
        ___qtablewidgetitem3 = self.tableFlirFiles.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("MainWindow", u"\u6a94\u6848\u540d\u7a31", None));
        ___qtablewidgetitem4 = self.tableFlirFiles.horizontalHeaderItem(4)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("MainWindow", u"Path", None));
        self.btnLoad2.setText(QCoreApplication.translate("MainWindow", u"Load", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Color Map :", None))
        self.cbColorMap.setItemText(0, QCoreApplication.translate("MainWindow", u"AUTUMN", None))
        self.cbColorMap.setItemText(1, QCoreApplication.translate("MainWindow", u"BONE", None))
        self.cbColorMap.setItemText(2, QCoreApplication.translate("MainWindow", u"JET", None))
        self.cbColorMap.setItemText(3, QCoreApplication.translate("MainWindow", u"WINTER", None))
        self.cbColorMap.setItemText(4, QCoreApplication.translate("MainWindow", u"RAINBOW", None))
        self.cbColorMap.setItemText(5, QCoreApplication.translate("MainWindow", u"OCEAN", None))
        self.cbColorMap.setItemText(6, QCoreApplication.translate("MainWindow", u"SUMMER", None))
        self.cbColorMap.setItemText(7, QCoreApplication.translate("MainWindow", u"SPRING", None))
        self.cbColorMap.setItemText(8, QCoreApplication.translate("MainWindow", u"COOL", None))
        self.cbColorMap.setItemText(9, QCoreApplication.translate("MainWindow", u"HSV", None))
        self.cbColorMap.setItemText(10, QCoreApplication.translate("MainWindow", u"PINK", None))
        self.cbColorMap.setItemText(11, QCoreApplication.translate("MainWindow", u"HOT", None))
        self.cbColorMap.setItemText(12, QCoreApplication.translate("MainWindow", u"PARULA", None))
        self.cbColorMap.setItemText(13, QCoreApplication.translate("MainWindow", u"MAGMA", None))
        self.cbColorMap.setItemText(14, QCoreApplication.translate("MainWindow", u"INFERNO", None))
        self.cbColorMap.setItemText(15, QCoreApplication.translate("MainWindow", u"PLASMA", None))
        self.cbColorMap.setItemText(16, QCoreApplication.translate("MainWindow", u"VIRIDIS", None))
        self.cbColorMap.setItemText(17, QCoreApplication.translate("MainWindow", u"CIVIDIS", None))
        self.cbColorMap.setItemText(18, QCoreApplication.translate("MainWindow", u"TWILIGHT", None))
        self.cbColorMap.setItemText(19, QCoreApplication.translate("MainWindow", u"TWILIGHT_SHIFTED", None))
        self.cbColorMap.setItemText(20, QCoreApplication.translate("MainWindow", u"TURBO", None))
        self.cbColorMap.setItemText(21, QCoreApplication.translate("MainWindow", u"DEEPGREEN", None))

        self.cbColorMap.setPlaceholderText("")
        self.btnLoad.setText(QCoreApplication.translate("MainWindow", u"Load", None))
        self.btnHistogram.setText(QCoreApplication.translate("MainWindow", u"Histogram", None))
        self.btnCSV.setText(QCoreApplication.translate("MainWindow", u"CSV", None))
        self.btnExit.setText(QCoreApplication.translate("MainWindow", u"Exit", None))
        self.lblMinTemperature.setText(QCoreApplication.translate("MainWindow", u"Min Temperature", None))
        self.lblMaxTemperature.setText(QCoreApplication.translate("MainWindow", u"Max Temperature", None))
        self.btnReset.setText(QCoreApplication.translate("MainWindow", u"Reset", None))
    # retranslateUi

