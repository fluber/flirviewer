from ast import arguments
from ColorRuler import ColorRuler
from FlirManager import FlirManager
from MyCSVDialog import *
from MainWindow import *
import numpy as np
import cv2
import os, re, logging, time 
import logging.handlers
import configparser
import matplotlib.pyplot as plt
from PySide6.QtCore import Signal, QStandardPaths, QTimer
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QGraphicsPixmapItem, QGraphicsScene, QGraphicsView, QGridLayout, QHBoxLayout, QMessageBox, QSplashScreen
from datetime import datetime

class MyMainWindow(QMainWindow):

    onShowTemperature = Signal(int, int)
    onShowColorBarTemperature = Signal(int, int)

    def __init__(self) -> None:
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.m_image = None
        self.m_colorized_img = None
        self.m_color_bar_image = None
        self.m_color_ruler_image = None
        self.m_temperature_data = None
        self.m_temperature = 0.0
        self.m_fileName = None
        self.m_colorMapIndex = 0
        self.m_img_width = 640
        self.m_img_height = 480
        self.m_pixels = 0
        self.m_color_map = self.init_color_map()
        self.m_clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        self.m_flirManager = FlirManager()
        self.onShowTemperature.connect(self.show_temperature)
        self.onShowColorBarTemperature.connect(self.show_color_ruler_temperature)
        self.ui.btnPath.clicked.connect(self.pick_path)
        self.ui.btnSearch.clicked.connect(self.search_flir_raw_files)
        self.ui.tableFlirFiles.setColumnHidden(4, True)
        self.m_config = configparser.ConfigParser()
        self.m_config.read(os.path.join(os.path.dirname(os.path.abspath(__file__)), "flir.ini"))
        self.ui.cbColorMap.setCurrentIndex(int(self.m_config['Main']['color_map_index']))
        self.ui.edtFilrPath.setText(self.m_config['Main']['flir_raw_file_path'])
        self.ui.edtStartDate.setDate(datetime.strptime(self.m_config['Main']['start_date'], "%Y%m%d").date())
        self.ui.cbStartRow.setCurrentText(self.m_config['Main']['start_row'])
        self.ui.cbStartCol.setCurrentText(self.m_config['Main']['start_col'])
        self.ui.cbEndRow.setCurrentText(self.m_config['Main']['end_row'])
        self.ui.cbEndCol.setCurrentText(self.m_config['Main']['end_col'])

        self.ui.btnExit.clicked.connect(self.close)
        self.ui.btnLoad.clicked.connect(self.load_flir_file)
        self.ui.btnLoad2.clicked.connect(self.load_flir_file2)
        self.ui.btnReset.clicked.connect(self.reset_threshold)
        self.ui.btnHistogram.clicked.connect(self.show_histogram)
        self.ui.btnCSV.clicked.connect(self.show_csv)
        self.ui.cbColorMap.currentTextChanged.connect(self.change_color_map)
        #self.ui.tableFlirFiles.setStyleSheet("border-width: 0;")

        self.m_pixmap_1 = QGraphicsPixmapItem()
        self.m_graphicsView_1 = QGraphicsView(self)
        self.m_graphicsView_1.setStyleSheet("border-width: 0px;")
        self.m_graphicsView_1.setScene(QGraphicsScene(self))
        self.m_graphicsView_1.setScene(QGraphicsScene(self))
        self.m_graphicsView_1.scene().addItem(self.m_pixmap_1)
        self.m_graphicsView_1.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.m_graphicsView_1.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.m_pixmap_2 = QGraphicsPixmapItem()
        self.m_graphicsView_2 = QGraphicsView(self)
        self.m_graphicsView_2.setStyleSheet("border-width: 0px;")
        self.m_graphicsView_2.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.m_graphicsView_2.setMinimumWidth(50)
        self.m_graphicsView_2.setMaximumWidth(50)
        self.m_graphicsView_2.setScene(QGraphicsScene(self))
        self.m_graphicsView_2.scene().addItem(self.m_pixmap_2)
        self.m_graphicsView_2.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.m_graphicsView_2.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)


        self.m_color_ruler = ColorRuler(self)
        self.m_color_ruler.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.m_color_ruler.setMaximumWidth(100)
        self.m_color_ruler.setMinimumWidth(100)

        self.m_layout = QHBoxLayout()
        self.ui.frame_3.setLayout(self.m_layout)
        self.m_layout.addWidget(self.m_graphicsView_1)
        self.m_layout.addWidget(self.m_graphicsView_2)
        self.m_layout.addWidget(self.m_color_ruler)
        self.center()

        self.ui.edtEndDate.setDate(datetime.now().date())

        #self.load_temperature()
        # self.showFullScreen()

    def center(self):
        qr = self.frameGeometry()
        cp = self.screen().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


    def closeEvent(self, event) -> None:
        self.save_ini()
        return super().closeEvent(event)

    def load_temperature(self):
        self.m_threshold_data = np.genfromtxt('temperature.csv', delimiter=',')

    def save_ini(self):
        self.m_config['Main']['start_date'] = self.ui.edtStartDate.dateTime().toString("yyyyMMdd")
        self.m_config['Main']['start_row'] = self.ui.cbStartRow.currentText()
        self.m_config['Main']['start_col'] = self.ui.cbStartCol.currentText()
        self.m_config['Main']['end_row'] = self.ui.cbEndRow.currentText()
        self.m_config['Main']['end_col'] = self.ui.cbEndCol.currentText()
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "flir.ini"),'w') as configfile:
            self.m_config.write(configfile) 

    def init_color_map(self):
        color_map = np.zeros([256, 10, 3], dtype=np.uint8)
        for i in range(256):
            for j in range(10):
                color_map[i, j, :] = int(255-i) 
        return color_map

    def threshold(self):
        self.ui.lblMinTemperature.setText('Min Temperature: {:.2f}'.format(self.ui.hsMinTemperature.value()))
        self.ui.lblMaxTemperature.setText('Max Temperature: {:.2f}'.format(self.ui.hsMaxTemperature.value()))
        min = self.ui.hsMinTemperature.value()
        max = self.ui.hsMaxTemperature.value()
        if self.m_temperature_data is not None and self.m_colorized_img is not None:
            result = (np.logical_and(self.m_temperature_data >= min, self.m_temperature_data <= max) * 255).astype(np.uint8)
            _, contours, _ = cv2.findContours(result, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            temp = self.m_colorized_img.copy()
            h,w,_=temp.shape
            dummy = np.zeros((h,w), np.uint8)
            dummy_img = cv2.drawContours(dummy, contours, -1, 255, -1)
            img = cv2.bitwise_and(temp,temp,mask = dummy_img)

            #img = cv2.drawContours(temp, contours, -1, (0, 255, 0), -1)
            gray = ((img[:,:,1] == 255) * 1).astype(np.uint8)
            self.m_pixels = np.count_nonzero(gray > 0)
            #self.areaChanged.emit(pixels)
            self.m_img = QImage(img.data, img.shape[1], img.shape[0], img.shape[1] * 3, QImage.Format_RGB888) 
            self.m_graphicsView_1.scene().clear()
            self.m_pixmap_1 = QGraphicsPixmapItem(QPixmap.fromImage(self.m_img))
            self.m_pixmap_1.hoverMoveEvent = self.pixmap_mouse_move_event
            self.m_pixmap_1.setFlag(QGraphicsPixmapItem.ItemIsSelectable)
            self.m_pixmap_1.setAcceptHoverEvents(True)
            self.m_graphicsView_1.scene().addItem(self.m_pixmap_1)
            self.m_graphicsView_1.fitInView(self.m_pixmap_1, Qt.KeepAspectRatio)
            self.m_graphicsView_1.viewport().update()

    def load_flir_file(self):
        self.m_pixels = 0
        self.m_colorMapIndex = self.ui.cbColorMap.currentIndex() 
        self.m_fileName = self.m_flirManager.pickImage() 
        with open(self.m_fileName, 'rb') as rawing:
            data = np.fromfile(rawing, np.uint16)
            self.m_data = np.reshape(data, (self.m_img_height, self.m_img_width))
            self.m_temperature_data = self.m_data * 0.01 - 273.15
            max = np.max(self.m_temperature_data)
            min = np.min(self.m_temperature_data)
            self.ui.hsMinTemperature.setMinimum(int(min))
            self.ui.hsMinTemperature.setMaximum(int(max))
            self.ui.hsMinTemperature.setValue(min)
            self.ui.hsMaxTemperature.setMinimum(int(min))
            self.ui.hsMaxTemperature.setMaximum(int(max))
            self.ui.hsMaxTemperature.setValue(max)
            try: self.ui.hsMinTemperature.valueChanged.disconnect(self.threshold)
            except: pass
            try: self.ui.hsMaxTemperature.valueChanged.disconnect(self.threshold)
            except: pass
            self.ui.hsMinTemperature.valueChanged.connect(self.threshold)
            self.ui.hsMaxTemperature.valueChanged.connect(self.threshold)
            self.threshold()
            self.m_color_ruler.setTemeratureData(min, max)
            self.m_color_ruler.update()
        normed = cv2.normalize(self.m_data, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        cl1 = self.m_clahe.apply(normed)
        nor = cv2.cvtColor(cl1, cv2.COLOR_GRAY2BGR)
        self.m_colorized_img = cv2.applyColorMap(nor, int(self.m_colorMapIndex))
        self.m_img = QImage(self.m_colorized_img.data, self.m_colorized_img.shape[1], self.m_colorized_img.shape[0], self.m_colorized_img.shape[1] * 3, QImage.Format_RGB888) 
        self.m_graphicsView_1.scene().clear()
        self.m_pixmap_1 = QGraphicsPixmapItem(QPixmap.fromImage(self.m_img))
        self.m_pixmap_1.hoverMoveEvent = self.pixmap_mouse_move_event
        self.m_pixmap_1.setFlag(QGraphicsPixmapItem.ItemIsSelectable)
        self.m_pixmap_1.setAcceptHoverEvents(True)
        self.m_graphicsView_1.scene().addItem(self.m_pixmap_1)
        self.m_graphicsView_1.fitInView(self.m_pixmap_1, Qt.KeepAspectRatio)
        self.m_graphicsView_1.viewport().update()
        self.gen_color_bar()

    def load_flir_file2(self):
        self.m_pixels = 0
        self.m_colorMapIndex = self.ui.cbColorMap.currentIndex() 
        index = self.ui.tableFlirFiles.currentRow()
        if index >= 0:
            self.m_fileName = self.ui.tableFlirFiles.item(index, 4).text()
            row = -1
            col = -1
            with open(self.m_fileName, 'rb') as rawing:
                data = np.fromfile(rawing, np.uint16)
                self.m_data = np.reshape(data, (self.m_img_height, self.m_img_width))
                self.m_temperature_data = self.m_data * 0.01 - 273.15
                max = np.max(self.m_temperature_data)
                min = np.min(self.m_temperature_data)
                self.ui.hsMinTemperature.setMinimum(int(min))
                self.ui.hsMinTemperature.setMaximum(int(max))
                self.ui.hsMinTemperature.setValue(min)
                self.ui.hsMaxTemperature.setMinimum(int(min))
                self.ui.hsMaxTemperature.setMaximum(int(max))
                self.ui.hsMaxTemperature.setValue(max)
                try: self.ui.hsMinTemperature.valueChanged.disconnect(self.threshold)
                except: pass
                try: self.ui.hsMaxTemperature.valueChanged.disconnect(self.threshold)
                except: pass
                self.ui.hsMinTemperature.valueChanged.connect(self.threshold)
                self.ui.hsMaxTemperature.valueChanged.connect(self.threshold)
                self.threshold()
                self.m_color_ruler.setTemeratureData(min, max)
                self.m_color_ruler.update()
            normed = cv2.normalize(self.m_data, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
            cl1 = self.m_clahe.apply(normed)
            nor = cv2.cvtColor(cl1, cv2.COLOR_GRAY2BGR)
            self.m_colorized_img = cv2.applyColorMap(nor, int(self.m_colorMapIndex))
            self.m_img = QImage(self.m_colorized_img.data, self.m_colorized_img.shape[1], self.m_colorized_img.shape[0], self.m_colorized_img.shape[1] * 3, QImage.Format_RGB888) 
            self.m_graphicsView_1.scene().clear()
            self.m_pixmap_1 = QGraphicsPixmapItem(QPixmap.fromImage(self.m_img))
            self.m_pixmap_1.hoverMoveEvent = self.pixmap_mouse_move_event
            self.m_pixmap_1.setFlag(QGraphicsPixmapItem.ItemIsSelectable)
            self.m_pixmap_1.setAcceptHoverEvents(True)
            self.m_graphicsView_1.scene().addItem(self.m_pixmap_1)
            self.m_graphicsView_1.fitInView(self.m_pixmap_1, Qt.KeepAspectRatio)
            self.m_graphicsView_1.viewport().update()
            self.gen_color_bar()
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle('Flir檢視程式')
            msg.setText('請先選取檔案！')
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()


    def pixmap_mouse_move_event(self, event):
        self.onShowTemperature.emit(event.pos().x(), event.pos().y())

    def pixmap_mouse_move_event_2(self, event):
        self.onShowColorBarTemperature.emit(event.pos().x(), event.pos().y())

    def reset_threshold(self):
        if self.m_temperature_data is not None:
            max = np.max(self.m_temperature_data)
            min = np.min(self.m_temperature_data)
            self.ui.hsMinTemperature.setValue(min)
            self.ui.hsMaxTemperature.setValue(max)
            normed = cv2.normalize(self.m_data, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
            cl1 = self.m_clahe.apply(normed)
            nor = cv2.cvtColor(cl1, cv2.COLOR_GRAY2BGR)
            self.m_colorized_img = cv2.applyColorMap(nor, int(self.m_colorMapIndex))
            self.m_img = QImage(self.m_colorized_img.data, self.m_colorized_img.shape[1], self.m_colorized_img.shape[0], self.m_colorized_img.shape[1] * 3, QImage.Format_RGB888) 
            self.m_graphicsView_1.scene().clear()
            self.m_pixmap_1 = QGraphicsPixmapItem(QPixmap.fromImage(self.m_img))
            self.m_pixmap_1.hoverMoveEvent = self.pixmap_mouse_move_event
            self.m_pixmap_1.setFlag(QGraphicsPixmapItem.ItemIsSelectable)
            self.m_pixmap_1.setAcceptHoverEvents(True)
            self.m_graphicsView_1.scene().addItem(self.m_pixmap_1)
            self.m_graphicsView_1.fitInView(self.m_pixmap_1, Qt.KeepAspectRatio)
            self.m_graphicsView_1.viewport().update()
            self.m_pixels = 0
            self.gen_color_bar()

    def show_temperature(self, x, y):
        if self.m_temperature_data is not None:
            if self.m_pixels > 0:
                content = "temperature: {:.2f}, area: {}".format(self.m_temperature_data[y, x], self.m_pixels)
            else:
                content = "temperature: {:.2f}".format(self.m_temperature_data[y, x])
            self.m_pixmap_1.setToolTip(content)

    def pick_path(self):
        path = QFileDialog.getExistingDirectory(self, "Open Directory",QStandardPaths.standardLocations(QStandardPaths.DocumentsLocation)[0], QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks)
        if len(path) > 0:
            self.m_config['Main']['flir_raw_file_path'] = path
            self.ui.edtFilrPath.setText(path)

    def search_flir_raw_files(self):
        try:
            self.ui.tableFlirFiles.setSelectionBehavior(QTableView.SelectRows)
            self.ui.tableFlirFiles.setRowCount(0)
            start_date = self.ui.edtStartDate.date()
            end_date = self.ui.edtEndDate.date()
            start_cell = '{0:02d}{1:02d}'.format(int(self.ui.cbStartRow.currentText()), int(self.ui.cbStartCol.currentText()))
            end_cell = '{0:02d}{1:02d}'.format(int(self.ui.cbEndRow.currentText()), int(self.ui.cbEndCol.currentText()))
            rootdir = self.m_config['Main']['flir_raw_file_path']
            regex = re.compile(r"(\d{8})_\d{6}_\d_(\d{4}).*raw$")
            for root, dirs, files in os.walk(rootdir):
                for file in files:
                    m = regex.match(file)
                    if m:
                        file_name = os.path.join(root, file)
                        if ((os.path.getsize(file_name) / 1000) < 1024):
                            file_date = datetime.strptime(m.groups()[0], "%Y%m%d").date()
                            file_cell = m.groups()[1]
                            if file_date >= start_date and file_date <= end_date:
                                if file_cell >= start_cell and file_cell <= end_cell:
                                    row_count = self.ui.tableFlirFiles.rowCount()
                                    self.ui.tableFlirFiles.insertRow(row_count)
                                    self.ui.tableFlirFiles.setItem(row_count, 0, QTableWidgetItem(file_date.strftime('%Y%m%d')))
                                    self.ui.tableFlirFiles.setItem(row_count, 1, QTableWidgetItem(file_cell[0:2]))
                                    self.ui.tableFlirFiles.setItem(row_count, 2, QTableWidgetItem(file_cell[2:4]))
                                    self.ui.tableFlirFiles.setItem(row_count, 3, QTableWidgetItem(file))
                                    self.ui.tableFlirFiles.setItem(row_count, 4, QTableWidgetItem(os.path.join(root, file)))
            header = self.ui.tableFlirFiles.horizontalHeader()       
            header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
            header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
            header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
            header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        except Exception as ex:
            logging.error("Catch an exception.", exc_info=True)


    def show_color_ruler_temperature(self, x, y):
        if self.m_color_map is not None:
            content = "temperature: {:.2f}".format(self.m_color_ruler.getTemperatureData(y))
            self.m_pixmap_2.setToolTip(content)
    
    def show_histogram(self):
        if self.m_temperature_data is not None:
            plt.title("Temperature Histogram")
            plt.hist(self.m_temperature_data.flatten(), color='blue')
            plt.show()

    def show_csv(self):
        if self.m_temperature_data is not None:
            min = self.ui.hsMinTemperature.value()
            max = self.ui.hsMaxTemperature.value()
            dialog = MyCSVDialog(self.m_temperature_data, min, max)
            dialog.exec()

    def change_color_map(self, value):
        if self.m_fileName is not None:
            self.m_colorMapIndex = self.ui.cbColorMap.currentIndex() 
            self.m_config['Main']['color_map_index'] = str(self.m_colorMapIndex)
            
            with open(self.m_fileName, 'rb') as rawing:
                data = np.fromfile(rawing, np.uint16)
                self.m_data = np.reshape(data, (self.m_img_height, self.m_img_width))
                self.m_temperature_data = self.m_data * 0.01 - 273.15
            normed = cv2.normalize(self.m_data, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
            cl1 = self.m_clahe.apply(normed)
            nor = cv2.cvtColor(cl1, cv2.COLOR_GRAY2BGR)
            self.m_colorized_img = cv2.applyColorMap(nor, int(self.m_colorMapIndex))
            self.m_img = QImage(self.m_colorized_img.data, self.m_colorized_img.shape[1], self.m_colorized_img.shape[0], self.m_colorized_img.shape[1] * 3, QImage.Format_RGB888) 
            self.m_graphicsView_1.scene().clear()
            self.m_pixmap_1 = QGraphicsPixmapItem(QPixmap.fromImage(self.m_img))
            self.m_pixmap_1.hoverMoveEvent = self.pixmap_mouse_move_event
            self.m_pixmap_1.setFlag(QGraphicsPixmapItem.ItemIsSelectable)
            self.m_pixmap_1.setAcceptHoverEvents(True)
            self.m_graphicsView_1.scene().addItem(self.m_pixmap_1)
            self.m_graphicsView_1.fitInView(self.m_pixmap_1, Qt.KeepAspectRatio)
            self.m_graphicsView_1.viewport().update()
            self.gen_color_bar()

    def gen_color_bar(self):
        colorized_img = cv2.applyColorMap(self.m_color_map, int(self.m_colorMapIndex))
        self.m_color_bar_image = QImage(colorized_img.data, colorized_img.shape[1], colorized_img.shape[0], colorized_img.shape[1] * 3, QImage.Format_RGB888) 
        self.m_graphicsView_2.scene().clear()
        self.m_pixmap_2 = QGraphicsPixmapItem(QPixmap.fromImage(self.m_color_bar_image))
        self.m_pixmap_2.hoverMoveEvent = self.pixmap_mouse_move_event_2
        self.m_pixmap_2.setFlag(QGraphicsPixmapItem.ItemIsSelectable)
        self.m_pixmap_2.setAcceptHoverEvents(True)
        self.m_graphicsView_2.scene().addItem(self.m_pixmap_2)
        self.m_graphicsView_2.fitInView(self.m_pixmap_2, Qt.KeepAspectRatio)
        self.m_graphicsView_2.viewport().update()
    
    def show_copyright(self):
        copyright_image = "{}\\images\\copyright.png".format(os.path.dirname(os.path.abspath(__file__)))
        self.m_copyright = QSplashScreen(QPixmap(copyright_image))
        self.m_copyright.show()
        QTimer.singleShot(8000, self.m_copyright.close)
    



if __name__ == "__main__":
    import sys
    arguments = sys.argv
    app = QApplication(arguments)
    log_dir = "{}/log".format(os.path.dirname(os.path.abspath(__file__)))
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_file = "{}/log.txt".format(log_dir)

    formatter = logging.Formatter('%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s')
    formatter.converter = time.gmtime

    fileHandler = logging.handlers.RotatingFileHandler(log_file, maxBytes=1024 * 1024 * 5, backupCount=1000)
    fileHandler.setFormatter(formatter)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)

    logger = logging.getLogger()
    logger.addHandler(fileHandler)
    logger.addHandler(consoleHandler)
    logger.setLevel(logging.ERROR)
    MainWindow = MyMainWindow()
    MainWindow.show()
    MainWindow.show_copyright()
    sys.exit(app.exec())