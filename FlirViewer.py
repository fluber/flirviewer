from msilib.schema import Property
from typing import Optional
import os, re
from PySide6.QtCore import Slot, Property, Signal, QSizeF, Qt, QRect, qAbs,QAbstractTableModel
from PySide6.QtGui import QImage, QPainter, QMouseEvent, QHoverEvent, QRgba64
from PySide6.QtQuick import QQuickItem, QQuickPaintedItem
import logging
import cv2
import numpy as np
from math import sqrt, exp, log
import matplotlib.pyplot as plt
from MyCSVDialog import *

def searchDirectory(cwd,searchParam,searchResults):
    dirs = os.listdir(cwd)
    for dir in dirs:
        fullpath = os.path.join(cwd,dir)
        if os.path.isdir(fullpath):
            searchDirectory(fullpath,searchParam,searchResults)
        if re.search(searchParam,fullpath):
            searchResults.append(fullpath)


# https://groups.google.com/g/flir-lepton/c/Cm8lGQyspmk
def generateColourMap():
    """
    Conversion of the colour map from GetThermal to a numpy LUT:
        https://github.com/groupgets/GetThermal/blob/bb467924750a686cc3930f7e3a253818b755a2c0/src/dataformatter.cpp#L6
    """

    lut = np.zeros((256, 1, 3), dtype=np.uint8)

    colormapIronBlack = [
        255, 255, 255, 253, 253, 253, 251, 251, 251, 249, 249, 249, 247, 247,
        247, 245, 245, 245, 243, 243, 243, 241, 241, 241, 239, 239, 239, 237,
        237, 237, 235, 235, 235, 233, 233, 233, 231, 231, 231, 229, 229, 229,
        227, 227, 227, 225, 225, 225, 223, 223, 223, 221, 221, 221, 219, 219,
        219, 217, 217, 217, 215, 215, 215, 213, 213, 213, 211, 211, 211, 209,
        209, 209, 207, 207, 207, 205, 205, 205, 203, 203, 203, 201, 201, 201,
        199, 199, 199, 197, 197, 197, 195, 195, 195, 193, 193, 193, 191, 191,
        191, 189, 189, 189, 187, 187, 187, 185, 185, 185, 183, 183, 183, 181,
        181, 181, 179, 179, 179, 177, 177, 177, 175, 175, 175, 173, 173, 173,
        171, 171, 171, 169, 169, 169, 167, 167, 167, 165, 165, 165, 163, 163,
        163, 161, 161, 161, 159, 159, 159, 157, 157, 157, 155, 155, 155, 153,
        153, 153, 151, 151, 151, 149, 149, 149, 147, 147, 147, 145, 145, 145,
        143, 143, 143, 141, 141, 141, 139, 139, 139, 137, 137, 137, 135, 135,
        135, 133, 133, 133, 131, 131, 131, 129, 129, 129, 126, 126, 126, 124,
        124, 124, 122, 122, 122, 120, 120, 120, 118, 118, 118, 116, 116, 116,
        114, 114, 114, 112, 112, 112, 110, 110, 110, 108, 108, 108, 106, 106,
        106, 104, 104, 104, 102, 102, 102, 100, 100, 100, 98, 98, 98, 96, 96,
        96, 94, 94, 94, 92, 92, 92, 90, 90, 90, 88, 88, 88, 86, 86, 86, 84, 84,
        84, 82, 82, 82, 80, 80, 80, 78, 78, 78, 76, 76, 76, 74, 74, 74, 72, 72,
        72, 70, 70, 70, 68, 68, 68, 66, 66, 66, 64, 64, 64, 62, 62, 62, 60, 60,
        60, 58, 58, 58, 56, 56, 56, 54, 54, 54, 52, 52, 52, 50, 50, 50, 48, 48,
        48, 46, 46, 46, 44, 44, 44, 42, 42, 42, 40, 40, 40, 38, 38, 38, 36, 36,
        36, 34, 34, 34, 32, 32, 32, 30, 30, 30, 28, 28, 28, 26, 26, 26, 24, 24,
        24, 22, 22, 22, 20, 20, 20, 18, 18, 18, 16, 16, 16, 14, 14, 14, 12, 12,
        12, 10, 10, 10, 8, 8, 8, 6, 6, 6, 4, 4, 4, 2, 2, 2, 0, 0, 0, 0, 0, 9,
        2, 0, 16, 4, 0, 24, 6, 0, 31, 8, 0, 38, 10, 0, 45, 12, 0, 53, 14, 0,
        60, 17, 0, 67, 19, 0, 74, 21, 0, 82, 23, 0, 89, 25, 0, 96, 27, 0, 103,
        29, 0, 111, 31, 0, 118, 36, 0, 120, 41, 0, 121, 46, 0, 122, 51, 0, 123,
        56, 0, 124, 61, 0, 125, 66, 0, 126, 71, 0, 127, 76, 1, 128, 81, 1, 129,
        86, 1, 130, 91, 1, 131, 96, 1, 132, 101, 1, 133, 106, 1, 134, 111, 1,
        135, 116, 1, 136, 121, 1, 136, 125, 2, 137, 130, 2, 137, 135, 3, 137,
        139, 3, 138, 144, 3, 138, 149, 4, 138, 153, 4, 139, 158, 5, 139, 163,
        5, 139, 167, 5, 140, 172, 6, 140, 177, 6, 140, 181, 7, 141, 186, 7,
        141, 189, 10, 137, 191, 13, 132, 194, 16, 127, 196, 19, 121, 198, 22,
        116, 200, 25, 111, 203, 28, 106, 205, 31, 101, 207, 34, 95, 209, 37,
        90, 212, 40, 85, 214, 43, 80, 216, 46, 75, 218, 49, 69, 221, 52, 64,
        223, 55, 59, 224, 57, 49, 225, 60, 47, 226, 64, 44, 227, 67, 42, 228,
        71, 39, 229, 74, 37, 230, 78, 34, 231, 81, 32, 231, 85, 29, 232, 88,
        27, 233, 92, 24, 234, 95, 22, 235, 99, 19, 236, 102, 17, 237, 106, 14,
        238, 109, 12, 239, 112, 12, 240, 116, 12, 240, 119, 12, 241, 123, 12,
        241, 127, 12, 242, 130, 12, 242, 134, 12, 243, 138, 12, 243, 141, 13,
        244, 145, 13, 244, 149, 13, 245, 152, 13, 245, 156, 13, 246, 160, 13,
        246, 163, 13, 247, 167, 13, 247, 171, 13, 248, 175, 14, 248, 178, 15,
        249, 182, 16, 249, 185, 18, 250, 189, 19, 250, 192, 20, 251, 196, 21,
        251, 199, 22, 252, 203, 23, 252, 206, 24, 253, 210, 25, 253, 213, 27,
        254, 217, 28, 254, 220, 29, 255, 224, 30, 255, 227, 39, 255, 229, 53,
        255, 231, 67, 255, 233, 81, 255, 234, 95, 255, 236, 109, 255, 238, 123,
        255, 240, 137, 255, 242, 151, 255, 244, 165, 255, 246, 179, 255, 248,
        193, 255, 249, 207, 255, 251, 221, 255, 253, 235, 255, 255, 24]

    def colormapChunk(ulist, step):
        return map(lambda i: ulist[i: i + step], range(0, len(ulist), step))

    chunks = colormapChunk(colormapIronBlack, 3)

    red = []
    green = []
    blue = []

    for chunk in chunks:
        red.append(chunk[0])
        green.append(chunk[1])
        blue.append(chunk[2])

    lut[:, 0, 0] = blue
    lut[:, 0, 1] = green
    lut[:, 0, 2] = red

    return lut

class TableModel(QAbstractTableModel):

    def __init__(self, data):
        super(TableModel, self).__init__()
        self._data = data

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the nested-list data structure.
            # .row() indexes into the outer list,
            # .column() indexes into the sub-list
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        # The length of the outer list.
        return len(self._data)

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        return len(self._data[0])

class FlirViewer(QQuickPaintedItem):

    temperatureChanged = Signal(float, arguments="temperature")
    areaChanged = Signal(int, arguments="area")

    def __init__(self, parent: Optional[QQuickItem] = None) -> None:
        super().__init__(parent)
        self.m_colorMapIndex = 0
        self.m_colorized_img = None
        self.m_fileName = None
        self.m_img = QImage()
        self.m_data = None
        self.m_temperature_data = None
        self.m_scaled = None
        self.m_centerRect = None
        self.m_img_width = 640
        self.m_img_height = 480
        self.m_temperature = 0.0
        self.m_color_map = generateColourMap()
        self.m_clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        self.setAcceptHoverEvents(True)
        self.setAcceptedMouseButtons(Qt.AllButtons)

    @Property(float, notify=temperatureChanged)
    def temperature(self):
        return self.m_temperature

    def setTemperature(self, val):
        self.m_temperature = val
        self.temperatureChanged.emit(val)

    @Slot(int)
    def setColorMap(self, index):
        if self.m_fileName:
            self.loadImage(index, self.m_fileName)

    @Slot()
    def showHist(self):
        if self.m_temperature_data is not None:
            min = np.min(self.m_temperature_data)
            max = np.max(self.m_temperature_data)
            bins = list(range(int(min), int(max)+1))
            plt.title("Temperature Histogram")
            plt.hist(self.m_temperature_data.flatten(), bins)
            plt.show()

    @Slot()
    def showCSV(self):
        if self.m_temperature_data is not None:
            dialog = MyCSVDialog(self.m_temperature_data)
            dialog.exec()

    @Slot(result=QSizeF)
    def getTemperatureData(self):
        if self.m_temperature_data is not None:
            max = np.max(self.m_temperature_data)
            min = np.min(self.m_temperature_data)
        else:
            max = 0
            min = 0
        return  QSizeF(min, max) 

    @Slot(int, str)
    def loadImage(self, index, fileName):
        self.m_colorMapIndex = index
        self.m_fileName = fileName
        with open(self.m_fileName, 'rb') as rawing:
            data = np.fromfile(rawing, np.uint16)
            self.m_data = np.reshape(data, (self.m_img_height, self.m_img_width))
            self.m_temperature_data = self.m_data * 0.01 - 273.15
        normed = cv2.normalize(self.m_data, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        cl1 = self.m_clahe.apply(normed)
        nor = cv2.cvtColor(cl1, cv2.COLOR_GRAY2BGR)
        self.m_colorized_img = cv2.applyColorMap(nor, int(self.m_colorMapIndex))
        self.m_img = QImage(self.m_colorized_img.data, self.m_colorized_img.shape[1], self.m_colorized_img.shape[0], self.m_colorized_img.shape[1] * 3, QImage.Format_RGB888) 
        self.update()

    @Slot()
    def resetImage(self):
        with open(self.m_fileName, 'rb') as rawing:
            data = np.fromfile(rawing, np.uint16)
            self.m_data = np.reshape(data, (self.m_img_height, self.m_img_width))
            self.m_temperature_data = self.m_data * 0.01 - 273.15
        normed = cv2.normalize(self.m_data, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        cl1 = self.m_clahe.apply(normed)
        nor = cv2.cvtColor(cl1, cv2.COLOR_GRAY2BGR)
        self.m_colorized_img = cv2.applyColorMap(nor, int(self.m_colorMapIndex))
        self.m_img = QImage(self.m_colorized_img.data, self.m_colorized_img.shape[1], self.m_colorized_img.shape[0], self.m_colorized_img.shape[1] * 3, QImage.Format_RGB888) 
        self.update()

    @Slot(float, float, result=float)
    def threshold(self, min, max):
        if self.m_temperature_data is not None:
            result = (np.logical_and(self.m_temperature_data >= min, self.m_temperature_data <= max) * 255).astype(np.uint8)
            _, contours, _ = cv2.findContours(result, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            temp = self.m_colorized_img.copy()

            img = cv2.drawContours(temp, contours, -1, (0, 255, 0), -1)
            gray = ((img[:,:,1] == 255) * 1).astype(np.uint8)
            pixels = np.count_nonzero(gray > 0)
            self.areaChanged.emit(pixels)

            self.m_img = QImage(img.data, img.shape[1], img.shape[0], img.shape[1] * 3, QImage.Format_RGB888) 
            self.update()
        return 0.0


    
    def paint(self, painter: QPainter) -> None:
        try:
            self.m_scaled = QSizeF(self.m_img.width(), self.m_img.height()).scaled(self.boundingRect().size(), Qt.KeepAspectRatio)
            self.m_centerRect = QRect(qAbs(self.m_scaled.width() - self.width()) / 2.0, qAbs(self.m_scaled.height() - self.height()) / 2.0, self.m_scaled.width(), self.m_scaled.height())
            painter.drawImage(self.m_centerRect, self.m_img)
        except:
            logging.error("Catch an exception.", exc_info=True)

    def mouseReleaseEvent(self, event: QMouseEvent) -> None:
        event.accept()

    def mousePressEvent(self, event: QMouseEvent) -> None:
        event.accept()

    def mouseMoveEvent(self, event: QMouseEvent) -> None:
        event.accept()
        try:
            pos = event.localPos()
            x = int((pos.x() - self.m_centerRect.x()) / self.m_scaled.width() * self.m_img.width())
            y = int((pos.y() - self.m_centerRect.y()) / self.m_scaled.height() * self.m_img.height())
            if (x > 0 and x < self.m_img.width() and y > 0 and y < self.m_img.height()):
                self.setTemperature(self.m_temperature_data[y, x])
                print("{},{},{:0.2f}".format(x, y, self.m_temperature))
        except:
            logging.error("Catch an exception.", exc_info=True)
    
    def hoverEnterEvent(self, event: QHoverEvent) -> None:
        event.accept()
        self.setCursor(Qt.CrossCursor)

    def hoverLeaveEvent(self, event: QHoverEvent) -> None:
        event.accept()
        self.setCursor(Qt.ArrowCursor)

    def hoverMoveEvent(self, event: QHoverEvent) -> None:
        event.accept()
        try:
            pos = event.pos()
            x = int((pos.x() - self.m_centerRect.x()) / self.m_scaled.width() * self.m_img.width())
            y = int((pos.y() - self.m_centerRect.y()) / self.m_scaled.height() * self.m_img.height())
            if (x > 0 and x < self.m_img.width() and y > 0 and y < self.m_img.height()):
                self.setTemperature(self.m_temperature_data[y, x])
        except:
            logging.error("Catch an exception.", exc_info=True)

    
