import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 2.15
import Qt.labs.platform
import QtQuick.Dialogs 
import com.winerva.qmlcomponents 1.0

Item {
    width: 1524
    height: 868
    property alias flirViewer: flirViewer
    property alias colorBar: colorBar
    property alias btnLoad: btnLoad
    property alias btnHist: btnHist
    property alias btnCSV: btnCSV
    property alias btnExit: btnExit
    property alias sliderPickMaxTemperature: sliderPickMaxTemperature
    property alias sliderPickMinTemperature: sliderPickMinTemperature
    property alias btnResetTemperature: btnResetTemperature
    property alias txtMaxTemperature: txtMaxTemperature
    property alias txtMinTemperature: txtMinTemperature
    property alias lblTemperature: lblTemperature
    property alias lblArea: lblArea
    property alias drawingCanvas: drawingCanvas
    property alias lblTemperature2: lblTemperature2
    property alias cbColorMap: cbColorMap
    property alias selectionComponent: selectionComponent
    property alias btnPickFlirFolder: btnPickFlirFolder
    property alias txtFlirPath: txtFlirPath
    property alias txtStartDate: txtStartDate
    property alias txtEndDate: txtEndDate
    property var selection: undefined

    Rectangle{
        x: 10
        width: 490
        height: 850
        color: "#ffffff"
        radius: 5
        border.color: "#705697"
        border.width: 3

        Label {
            x: 10
            y: 20
            text: "Filr Path:"
        }

        Rectangle {
            x: 80
            y: 10
            height: 30
            width: 300
            color: "#ffffff"
            radius: 5
            border.color: "#705697"
            border.width: 3
            TextField {
                id: txtFlirPath
                anchors.fill: parent
                anchors.margins: 4
                placeholderText: qsTr("flir data folder")
                verticalAlignment: TextInput.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        Button {
            id: btnPickFlirFolder
            x: 390
            y: 10
            height: 30
            width: 30
            text: qsTr("...")
        }

        Label {
            x: 10
            y: 70
            text: "Start Date:"
        }

        Rectangle {
            x: 80
            y: 60
            height: 30
            width: 100
            color: "#ffffff"
            radius: 5
            border.color: "#705697"
            border.width: 3
            TextField {
                id: txtStartDate
                anchors.fill: parent
                anchors.margins: 4
                inputMask: "9999/99/99"
                verticalAlignment: TextInput.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                inputMethodHints: Qt.ImhDigitsOnly
            }
        }

        Label {
            x: 200
            y: 70
            text: "End Date:"
        }
        Rectangle {
            x: 270
            y: 60
            height: 30
            width: 100
            color: "#ffffff"
            radius: 5
            border.color: "#705697"
            border.width: 3
            TextField {
                id: txtEndDate
                anchors.fill: parent
                anchors.margins: 4
                inputMask: "9999/99/99"
                verticalAlignment: TextInput.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                inputMethodHints: Qt.ImhDigitsOnly
            }
        }

        Button {
            id: btnSearchFilrFile
            x: 390
            y: 60
            height: 36 
            width: 36
            Image {
                anchors.fill: parent
                anchors.margins: 2
                source: "images/find.png"
                fillMode: Image.Tile
            }
        }


        Rectangle {
            x: 10
            y: 100
            width: 470
            height: 730
            border {
                color: "#705697"
                width: 2
            }
            radius: 5.0
            ScrollView {
                anchors.fill: parent
                
                anchors.margins: 4
                
                clip: true
                ScrollBar.vertical.policy: ScrollBar.AlwaysOn
                ListView {
                    id: listViewFlirList
                    topMargin: 5
                    leftMargin: 5
                    add: Transition {
                        NumberAnimation { properties: "x,y"; from: 100; duration: 1000 }
                    }
                    remove: Transition {
                        ParallelAnimation {
                            NumberAnimation { property: "opacity"; to: 0; duration: 1000 }
                            NumberAnimation { properties: "x,y"; to: 100; duration: 1000 }
                        }
                    }
                    model: weeklyMotionPlanModel
                    spacing: 5
                    delegate: weeklyMotionPlanComponent

                }
            }
        }

    }

    Rectangle {
        id: rectControl
        x: 510
        width: 1000
        height: 100
        color: "#ffffff"
        radius: 5
        border.color: "#705697"
        border.width: 3

        ComboBox {
            id: cbColorMap
            x: 50
            y: 25
            width: 180 
            height: 50
            currentIndex: 0
            displayText: "Color Map: " + currentText
            //model: ["LUT", "BONE", "CIVIDIS", "DEEPGREEN", "OCEAN"]
            model: ["AUTUMN", "BONE", "JET", "WINTER", "RAINBOW", "OCEAN", "SUMMER", "SPRING", "COOL", "HSV", "PINK", "HOT", "PARULA", "MAGMA", "INFERNO", "PLASMA", "VIRIDIS", "CIVIDIS", "TWILIGHT", "TWILIGHT_SHIFTED", "TURBO", "DEEPGREEN"]
        }       
        
        Button {
            id: btnLoad
            x: 250
            y: 25
            width: 100 
            height: 50
            text: qsTr("Load ")
            highlighted: false
            display: AbstractButton.TextBesideIcon
        }

        Button {
            id: btnHist
            x: 500
            y: 25
            width: 100
            height: 50
            
            
            text: qsTr("Histogram")
        }

        Button {
            id: btnCSV
            x: 650
            y: 25
            width: 100
            height: 50
            
            
            text: qsTr("CSV")
        }

        Button {
            id: btnExit
            x: 850
            y: 25
            width: 100
            height: 50
            
            
            text: qsTr("Exit")
        }
    }
    Rectangle {
        id: thresholdControl
        width: 1000
        height: 100
        color: "#ffffff"
        radius: 5
        border.color: "#705697"
        border.width: 3
        anchors.horizontalCenter: rectControl.horizontalCenter
        anchors.top: rectControl.bottom
        anchors.margins: 10

        Label {
            x: 10
            y: 10
            text: "Max Temperature"
        }

        Text {
            id: txtMaxTemperature
            x: 120
            y: 10
            text: ""
        }

        Slider {
            id: sliderPickMaxTemperature
            x: 170
            y: 8 
            from: 0.00
            to: 100.00
            stepSize: 0.01
        }


        Label {
            x: 10
            y: 40
            text: "Min Temperature"
        }

        Text {
            id: txtMinTemperature
            x: 120
            y: 40
            text: ""
        }

        Slider {
            id: sliderPickMinTemperature
            x: 170
            y: 48 
            from: 0.00
            to: 100.00
            stepSize: 0.01
        }

        Button {
            id: btnResetTemperature
            x: 10
            y: 70 
            width: 150
            height: 20
            text: qsTr("Reset")
        }
    }
    Rectangle {
        id: rectView 
        width: 1000 
        height: 630
        color: "#ffffff"
        radius: 5
        border.color: "#705697"
        border.width: 3
        
        anchors.horizontalCenter: thresholdControl.horizontalCenter
        anchors.top: thresholdControl.bottom
        anchors.margins: 10

        RowLayout {

            anchors.fill: parent
            spacing: 6
            
            anchors.margins: 10
            

            Rectangle {
                Layout.fillWidth: true
                Layout.preferredWidth: 800
                Layout.minimumHeight: 600

                ColumnLayout {
                    spacing: 6
                    anchors.fill: parent

                    FlirViewer {
                        id: flirViewer
                        width: 640
                        height: 480
                        Layout.alignment: Qt.AlignCenter
                        Layout.fillWidth: true

                    }
                    Text {
                        id: lblTemperature
                        font.family: "Helvetica"
                        font.pointSize: 24
                        font.bold: true
                        Layout.alignment: Qt.AlignBottom | Qt.AlignHCenter
                        Layout.bottomMargin: 20
                    }

                    Text {
                        id: lblArea
                        font.family: "Helvetica"
                        font.pointSize: 24
                        font.bold: true
                        Layout.alignment: Qt.AlignHCenter
                    }

                }

            }


            Rectangle {
                //Layout.fillWidth: true
                Layout.preferredWidth: 100
                Layout.minimumHeight: 600
                RowLayout {
                    anchors.fill: parent
                    // anchors.margins: 2 
                    // spacing: 2
                     ColorBar {
                        id: colorBar
                        Layout.fillWidth: true
                        Layout.preferredHeight: parent.height - 15
                        Text {
                            id: lblTemperature2
                            font.family: "Helvetica"
                            font.pointSize: 12 
                            font.bold: true

                            anchors.horizontalCenter: colorBar.horizontalCenter
                            anchors.top: colorBar.bottom
                            anchors.topMargin: 1

                        }
                     }
                    Rectangle {
                        id: ruler
                        Layout.fillWidth: true
                        Layout.preferredHeight: parent.height 
                        Canvas
                        {
                            id: drawingCanvas
                            anchors.fill: parent
                            onPaint:
                            {
                                var ctx = getContext("2d")

                                ctx.fillStyle = "white"
                                var dh = drawingCanvas.height - 10 
                                ctx.fillRect(0,0,drawingCanvas.width , dh)

                                ctx.lineWidth = 1
                                ctx.strokeStyle = "red"
                                var offset_y = dh / 100
                                var y = offset_y 
                                ctx.beginPath()
                                for (var i = 0; i < 101; i++)
                                {
                                    var j = parseInt((i / 100) * 255) 
                                    var data = colorBar.getTemperatureData(j)
                                    ctx.moveTo(0, y)
                                    if (i % 5 == 0)
                                    {
                                        ctx.lineTo(drawingCanvas.width / 2, y)
                                        ctx.fillStyle = "black"
                                        ctx.fillText(data, 30, y)
                                        ctx.fillStyle = "white"
                                    }
                                    else
                                    {
                                        ctx.lineTo(drawingCanvas.width / 4, y)
                                    }
                                    y = y + offset_y
                                }
                                ctx.closePath()
                                ctx.stroke()
                            }
                        }
                    }
                }


            }
        }
    }


    Component {
        id: selectionComponent

        Rectangle {
            id: selComp
            border {
                width: 2
                color: "steelblue"
            }
            color: "#354682B4"

            property int rulersSize: 18

            MouseArea {     // drag mouse area
                anchors.fill: parent
                drag{
                    target: parent
                    minimumX: 0
                    minimumY: 0
                    maximumX: parent.parent.width - parent.width
                    maximumY: parent.parent.height - parent.height
                    smoothed: true
                }

                onDoubleClicked: {
                    parent.destroy()        // destroy component
                }
            }

            Rectangle {
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                color: "steelblue"
                anchors.horizontalCenter: parent.left
                anchors.verticalCenter: parent.verticalCenter

                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            selComp.width = selComp.width - mouseX
                            selComp.x = selComp.x + mouseX
                            if(selComp.width < 30)
                                selComp.width = 30
                        }
                    }
                }
            }

            Rectangle {
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                color: "steelblue"
                anchors.horizontalCenter: parent.right
                anchors.verticalCenter: parent.verticalCenter

                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            selComp.width = selComp.width + mouseX
                            if(selComp.width < 50)
                                selComp.width = 50
                        }
                    }
                }
            }

            Rectangle {
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                x: parent.x / 2
                y: 0
                color: "steelblue"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.top

                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.YAxis }
                    onMouseYChanged: {
                        if(drag.active){
                            selComp.height = selComp.height - mouseY
                            selComp.y = selComp.y + mouseY
                            if(selComp.height < 50)
                                selComp.height = 50
                        }
                    }
                }
            }

            Rectangle {
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                x: parent.x / 2
                y: parent.y
                color: "steelblue"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.bottom

                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.YAxis }
                    onMouseYChanged: {
                        if(drag.active){
                            selComp.height = selComp.height + mouseY
                            if(selComp.height < 50)
                                selComp.height = 50
                        }
                    }
                }
            }
        }
    }
    

}
