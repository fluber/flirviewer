import QtQuick 2.15
import com.winerva.qmlcomponents 1.0

MainForm {

    btnLoad.onClicked: {
        flirViewer.loadImage(cbColorMap.currentIndex, FlirManager.pickImage())
        colorBar.setColorMap(cbColorMap.currentIndex)
        colorBar.setTemperatureData(flirViewer.getTemperatureData())
        drawingCanvas.requestPaint()
    }

    btnHist.onClicked: {
        
        console.log(selectionComponent.x)
        
        flirViewer.showHist()
    }

    btnCSV.onClicked: {
        flirViewer.showCSV()
    }

    btnExit.onClicked: {
        FlirManager.exit()
    }

    btnResetTemperature.onClicked: {
        colorBar.setResetTemperature()
        lblArea.text = ''
    }

    btnPickFlirFolder.onClicked: {
        FlirManager.setFlirRawFilePath(FlirManager.pickFlirRawFilePath())
        txtFlirPath.text = FlirManager.getFlirRawFilePath()
    }



    Connections {
        target: cbColorMap
        function onActivated(index) {
            flirViewer.setColorMap(index)
            colorBar.setColorMap(index)
            
        }
    } 
    
    Connections {
        target: flirViewer
        function onTemperatureChanged(temperature) {
            lblTemperature.text = "Celsius: " + (temperature).toFixed(2) + "\u2103"
        }

        function onAreaChanged(area) {
            lblArea.text = "Area: " + area

        }
        
    }
    
    Connections {
        target: colorBar 
        function onTemperatureChanged(temperature) {
            lblTemperature2.text = "" + (temperature).toFixed(2) + "\u2103"
        }

        function onInitTemperature(min, max) {
            txtMaxTemperature.text = max.toFixed(2)
            txtMinTemperature.text = min.toFixed(2)

            sliderPickMinTemperature.from = min.toFixed(2)
            sliderPickMinTemperature.to = max.toFixed(2)
            sliderPickMinTemperature.value = min.toFixed(2)
            
            sliderPickMaxTemperature.from = min.toFixed(2)
            sliderPickMaxTemperature.to = max.toFixed(2)
            sliderPickMaxTemperature.value = max.toFixed(2)

            flirViewer.resetImage()

        }
        
    }
    
    Connections {
        target: sliderPickMinTemperature
        function onMoved() {
            txtMinTemperature.text = sliderPickMinTemperature.value.toFixed(2)
            flirViewer.threshold(parseFloat(txtMinTemperature.text), parseFloat(txtMaxTemperature.text))
        } 
    }
    
    Connections {
        target: sliderPickMaxTemperature
        function onMoved() {
            txtMaxTemperature.text = sliderPickMaxTemperature.value.toFixed(2)
            flirViewer.threshold(parseFloat(txtMinTemperature.text), parseFloat(txtMaxTemperature.text))
        } 
    }
    
    Component.onCompleted: {
        txtFlirPath.text = FlirManager.getFlirRawFilePath()
        txtStartDate.text = FlirManager.getStartDate()
        txtEndDate.text = FlirManager.getEndDate()
    }
}