from CSVDialog import *
from PySide6.QtCore import QAbstractTableModel, QStandardPaths
from PySide6.QtWidgets import QFileDialog
import numpy as np

class TableModel(QAbstractTableModel):
    def __init__(self, data):
        super(TableModel, self).__init__()
        self._data = data

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the nested-list data structure.
            # .row() indexes into the outer list,
            # .column() indexes into the sub-list
            return "{:.2f}".format(self._data[index.row()][index.column()])

    def rowCount(self, index):
        # The length of the outer list.
        return len(self._data)

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        return len(self._data[0])

class MyCSVDialog(QDialog):
    def __init__(self, data, min, max) -> None:
        super().__init__()
        self.ui = Ui_CSVDialog()
        self.ui.setupUi(self)
        data[data < min] = 0
        data[data > max] = 0
        self.m_data = data 
        self.m_model = TableModel(self.m_data)
        self.ui.tableView.setModel(self.m_model)
        for i in range(10):
            self.ui.tableView.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)
        self.ui.btnCSV.clicked.connect(self.export_csv)
        self.center()

    def center(self):
        qr = self.frameGeometry()
        cp = self.screen().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())
        
    def to_str(self, var):
        return str(list(np.reshape(np.asarray(var), (1, np.size(var)))[0]))[1:-1]

    def export_csv(self):
        save_dir =  QStandardPaths.writableLocation(QStandardPaths.DocumentsLocation)
        name = QFileDialog.getSaveFileName(self, 'Save File', save_dir,'*.csv')
        np.savetxt(name[0], self.m_data, delimiter=',', fmt='%.2f')