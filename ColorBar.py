from msilib.schema import Property
from tokenize import Single
from typing import Optional

from PySide6.QtCore import Slot, Property, Signal, QSizeF, Qt, QRect, qAbs
from PySide6.QtGui import QImage, QPainter, QMouseEvent, QHoverEvent, QRgba64
from PySide6.QtQuick import QQuickItem, QQuickPaintedItem
import logging
import cv2
import numpy as np
import matplotlib.pyplot as plt


class ColorBar(QQuickPaintedItem):

    temperatureChanged = Signal(float, arguments="temperature")
    initTemperature = Signal(float, float, arguments="min,max")

    def __init__(self, parent: Optional[QQuickItem] = None) -> None:
        super().__init__(parent)
        self.m_temperature_data = None
        self.m_temperature = 0.0
        self.m_img = QImage()
        self.m_color_map = self.init_color_map()
        self.setAcceptHoverEvents(True)
        self.setAcceptedMouseButtons(Qt.AllButtons)

    @Property(float, notify=temperatureChanged)
    def temperature(self):
        return self.m_temperature

    def setTemperature(self, val):
        self.m_temperature = val
        self.temperatureChanged.emit(val)

    def init_color_map(self):
        color_map = np.zeros([256, 10, 3], dtype=np.uint8)
        for i in range(256):
            for j in range(10):
                color_map[i, j, :] = int(255-i) 
        return color_map

    @Slot(QSizeF)
    def setTemperatureData(self, val):
        self.m_min_temperature = val.width()
        self.m_max_temperature = val.height()
        step = (self.m_max_temperature - self.m_min_temperature) / 256 * -1
        self.m_temperature_data = np.arange(self.m_max_temperature, self.m_min_temperature, step)
        self.initTemperature.emit(self.m_min_temperature, self.m_max_temperature)

    @Slot(int, result=str)
    def getTemperatureData(self, index):
        if self.m_temperature_data is not None:
            return str(self.m_temperature_data[index])
        else:
            return ''
            
    @Slot(int)
    def setColorMap(self, index):
        self.m_colorMapIndex = index
        self.genImage()

    @Slot(str)
    def genImage(self):
        colorized_img = cv2.applyColorMap(self.m_color_map, int(self.m_colorMapIndex))
        self.m_img = QImage(colorized_img.data, colorized_img.shape[1], colorized_img.shape[0], colorized_img.shape[1] * 3, QImage.Format_RGB888) 
        self.update()

    @Slot()
    def setResetTemperature(self):
        self.initTemperature.emit(self.m_min_temperature, self.m_max_temperature)

    
    def paint(self, painter: QPainter) -> None:
        try:
            self.m_scaled = QSizeF(self.m_img.width(), self.m_img.height()).scaled(self.boundingRect().size(), Qt.KeepAspectRatio)
            self.m_centerRect = QRect(qAbs(self.m_scaled.width() - self.width()) / 2.0, qAbs(self.m_scaled.height() - self.height()) / 2.0, self.m_scaled.width(), self.m_scaled.height())
            painter.drawImage(self.m_centerRect, self.m_img)
        except:
            logging.error("Catch an exception.", exc_info=True)

    def mouseReleaseEvent(self, event: QMouseEvent) -> None:
        event.accept()

    def mousePressEvent(self, event: QMouseEvent) -> None:
        event.accept()

    def mouseMoveEvent(self, event: QMouseEvent) -> None:
        event.accept()
        try:
            pos = event.localPos()
            x = int((pos.x() - self.m_centerRect.x()) / self.m_scaled.width() * self.m_img.width())
            y = int((pos.y() - self.m_centerRect.y()) / self.m_scaled.height() * self.m_img.height())
            if (x > 0 and x < self.m_img.width() and y > 0 and y < self.m_img.height()):
                self.setTemperature(self.m_temperature_data[y])
                print("{:0.2f}".format(self.m_temperature))
        except:
            logging.error("Catch an exception.", exc_info=True)
    
    def hoverEnterEvent(self, event: QHoverEvent) -> None:
        event.accept()
        self.setCursor(Qt.CrossCursor)

    def hoverLeaveEvent(self, event: QHoverEvent) -> None:
        event.accept()
        self.setCursor(Qt.ArrowCursor)

    def hoverMoveEvent(self, event: QHoverEvent) -> None:
        event.accept()
        try:
            pos = event.pos()
            x = int((pos.x() - self.m_centerRect.x()) / self.m_scaled.width() * self.m_img.width())
            y = int((pos.y() - self.m_centerRect.y()) / self.m_scaled.height() * self.m_img.height())
            if self.m_temperature_data is not None:
                if (x > 0 and x < self.m_img.width() and y > 0 and y < self.m_img.height()):
                    self.setTemperature(self.m_temperature_data[y])
        except:
            logging.error("Catch an exception.", exc_info=True)

    
