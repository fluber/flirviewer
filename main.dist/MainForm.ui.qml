import QtQuick 2.15
import QtQuick.Controls 2.15
import com.winerva.qmlcomponents 1.0

Item {
    width: 1024
    height: 768
    property alias flirViewer: flirViewer
    property alias btnLoad: btnLoad
    property alias btnHist: btnHist
    property alias btnExit: btnExit
    property alias lblTemperature: lblTemperature
    property alias cbColorMap: cbColorMap
    property alias selectionComponent: selectionComponent
    property var selection: undefined

    Rectangle {
        id: rectControl
        x: 10
        width: 1000
        height: 100
        color: "#ffffff"
        radius: 5
        border.color: "#705697"
        border.width: 3

        ComboBox {
            id: cbColorMap
            x: 50
            y: 25
            width: 180 
            height: 50
            currentIndex: 1
            displayText: "Color Map: " + currentText
            model: ["LUT", "BONE", "CIVIDIS", "DEEPGREEN", "OCEAN"]
        }       
        
        Button {
            id: btnLoad
            x: 250
            y: 25
            width: 100 
            height: 50
            text: qsTr("Load ")
            highlighted: false
            display: AbstractButton.TextBesideIcon
        }

        Button {
            id: btnHist
            x: 550
            y: 25
            width: 100
            height: 50
            
            
            text: qsTr("Histogram")
        }

        Button {
            id: btnExit
            x: 850
            y: 25
            width: 100
            height: 50
            
            
            text: qsTr("Exit")
        }
    }

    Rectangle {
        id: rectView 
        width: 1000 
        height: 630
        color: "#ffffff"
        radius: 5
        border.color: "#705697"
        border.width: 3
        
        anchors.horizontalCenter: rectControl.horizontalCenter
        anchors.top: rectControl.bottom
        anchors.margins: 10
        
        FlirViewer {
            id: flirViewer
            width: 640
            height: 480
            
            anchors.centerIn: parent
            
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(!selection)
                        selection = selectionComponent.createObject(parent, {"x": parent.width / 4, "y": parent.height / 4, "width": parent.width / 2, "height": parent.width / 2})
                }
            }

        }
            //anchors.horizontalCenter: parent.horizontalCenter
        Text {
            id: lblTemperature
            font.family: "Helvetica"
            font.pointSize: 24
            font.bold: true
            
            anchors.horizontalCenter: flirViewer.horizontalCenter
            anchors.top: flirViewer.bottom
            anchors.topMargin: 5
            
        }
            
    }


    Component {
        id: selectionComponent

        Rectangle {
            id: selComp
            border {
                width: 2
                color: "steelblue"
            }
            color: "#354682B4"

            property int rulersSize: 18

            MouseArea {     // drag mouse area
                anchors.fill: parent
                drag{
                    target: parent
                    minimumX: 0
                    minimumY: 0
                    maximumX: parent.parent.width - parent.width
                    maximumY: parent.parent.height - parent.height
                    smoothed: true
                }

                onDoubleClicked: {
                    parent.destroy()        // destroy component
                }
            }

            Rectangle {
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                color: "steelblue"
                anchors.horizontalCenter: parent.left
                anchors.verticalCenter: parent.verticalCenter

                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            selComp.width = selComp.width - mouseX
                            selComp.x = selComp.x + mouseX
                            if(selComp.width < 30)
                                selComp.width = 30
                        }
                    }
                }
            }

            Rectangle {
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                color: "steelblue"
                anchors.horizontalCenter: parent.right
                anchors.verticalCenter: parent.verticalCenter

                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            selComp.width = selComp.width + mouseX
                            if(selComp.width < 50)
                                selComp.width = 50
                        }
                    }
                }
            }

            Rectangle {
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                x: parent.x / 2
                y: 0
                color: "steelblue"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.top

                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.YAxis }
                    onMouseYChanged: {
                        if(drag.active){
                            selComp.height = selComp.height - mouseY
                            selComp.y = selComp.y + mouseY
                            if(selComp.height < 50)
                                selComp.height = 50
                        }
                    }
                }
            }

            Rectangle {
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                x: parent.x / 2
                y: parent.y
                color: "steelblue"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.bottom

                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.YAxis }
                    onMouseYChanged: {
                        if(drag.active){
                            selComp.height = selComp.height + mouseY
                            if(selComp.height < 50)
                                selComp.height = 50
                        }
                    }
                }
            }
        }
    }
    

}
