import QtQuick 2.15
import com.winerva.qmlcomponents 1.0

MainForm {

    btnLoad.onClicked: {
        flirViewer.loadImage(FlirManager.pickImage())
    }

    btnHist.onClicked: {
        
        console.log(selectionComponent.x)
        
        flirViewer.showHist()
    }

    btnExit.onClicked: {
        FlirManager.exit()
    }

    Connections {
        target: cbColorMap
        function onActivated(index) {
            flirViewer.setColorMap(index)
            
        }
    } 
    
    Connections {
        target: flirViewer
        function onTemperatureChanged(temperature) {
            lblTemperature.text = "Celsius: " + (temperature).toFixed(2) + "\u2103"
        }
        
        //onTemperatureChanged: {
        //    lblTemperature.text = (flirViewer.temperature).toFixed(2)
        //}
    }
    
    
}