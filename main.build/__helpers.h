#ifndef __NUITKA_CALLS_H__
#define __NUITKA_CALLS_H__

extern PyObject *CALL_FUNCTION_WITH_ARGS11(PyObject *called, PyObject *const *args);
extern PyObject *CALL_FUNCTION_WITH_ARGS13(PyObject *called, PyObject *const *args);
extern PyObject *CALL_METHOD_WITH_ARGS13(PyObject *source, PyObject *attr_name, PyObject *const *args);
#endif
