# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'CSVDialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QDialog, QDialogButtonBox,
    QHeaderView, QPushButton, QSizePolicy, QTableView,
    QWidget)

class Ui_CSVDialog(object):
    def setupUi(self, CSVDialog):
        if not CSVDialog.objectName():
            CSVDialog.setObjectName(u"CSVDialog")
        CSVDialog.resize(1024, 868)
        font = QFont()
        font.setPointSize(18)
        CSVDialog.setFont(font)
        self.buttonBox = QDialogButtonBox(CSVDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setGeometry(QRect(660, 800, 341, 32))
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.tableView = QTableView(CSVDialog)
        self.tableView.setObjectName(u"tableView")
        self.tableView.setGeometry(QRect(20, 20, 971, 741))
        self.btnCSV = QPushButton(CSVDialog)
        self.btnCSV.setObjectName(u"btnCSV")
        self.btnCSV.setGeometry(QRect(10, 780, 241, 61))

        self.retranslateUi(CSVDialog)
        self.buttonBox.accepted.connect(CSVDialog.accept)
        self.buttonBox.rejected.connect(CSVDialog.reject)

        QMetaObject.connectSlotsByName(CSVDialog)
    # setupUi

    def retranslateUi(self, CSVDialog):
        CSVDialog.setWindowTitle(QCoreApplication.translate("CSVDialog", u"Dialog", None))
        self.btnCSV.setText(QCoreApplication.translate("CSVDialog", u"Generate CSV File", None))
    # retranslateUi

