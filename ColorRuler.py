from PySide6.QtCore import Qt, QRect
from PySide6.QtWidgets import QWidget, QSizePolicy
from PySide6.QtGui import QPainter, QColor, QFont
import numpy as np

class ColorRuler(QWidget):

    def __init__(self, parent):
        super().__init__(parent)
        self.m_temperature_data = None


    def setTemeratureData(self, min, max):
        step = (max - min) / 256 * -1
        self.m_temperature_data = np.arange(max, min, step)

    def getTemperatureData(self, index):
        if self.m_temperature_data is not None:
            return self.m_temperature_data[index]
        else:
            return 0

    def paintEvent(self, event) -> None:
        qp = QPainter()
        qp.begin(self)
        qp.setPen(QColor(255, 0, 0))
        dh = self.height() - 10
        offset_y = dh / 100
        y = offset_y
        interval = 256 / 100
        for i in range(100):
            if (i % 5 == 0):
                qp.drawLine(0, y, self.width() / 2, y)
                if self.m_temperature_data is not None:
                    qp.drawText(55, y+5, '{:.2f}'.format(self.m_temperature_data[int(i * interval)]))
            else:
                qp.drawLine(0, y, self.width() / 4, y)
            y += offset_y
        qp.end()
        # var ctx = getContext("2d")

        # ctx.fillStyle = "white"
        # var dh = drawingCanvas.height - 10 
        # ctx.fillRect(0,0,drawingCanvas.width , dh)

        # ctx.lineWidth = 1
        # ctx.strokeStyle = "red"
        # var offset_y = dh / 100
        # var y = offset_y 
        # ctx.beginPath()
        # for (var i = 0; i < 101; i++)
        # {
        #     var j = parseInt((i / 100) * 255) 
        #     var data = colorBar.getTemperatureData(j)
        #     ctx.moveTo(0, y)
        #     if (i % 5 == 0)
        #     {
        #         ctx.lineTo(drawingCanvas.width / 2, y)
        #         ctx.fillStyle = "black"
        #         ctx.fillText(data, 30, y)
        #         ctx.fillStyle = "white"
        #     }
        #     else
        #     {
        #         ctx.lineTo(drawingCanvas.width / 4, y)
        #     }
        #     y = y + offset_y
        # }
        # ctx.closePath()
        # ctx.stroke()

