import sys
from PySide6.QtWidgets import QApplication
from PySide6.QtQuick import QQuickView
from PySide6.QtQml import qmlRegisterType, qmlRegisterSingletonType
from ColorBar import ColorBar
from FlirViewer import FlirViewer
from FlirManager import FlirManager

if __name__ == "__main__":
    app = QApplication()
    view = QQuickView()
    qmlRegisterSingletonType(FlirManager, "com.winerva.qmlcomponents", 1, 0, "FlirManager")
    qmlRegisterType(FlirViewer, "com.winerva.qmlcomponents", 1, 0, "FlirViewer")
    qmlRegisterType(ColorBar, "com.winerva.qmlcomponents", 1, 0, "ColorBar")
    view.setSource("View.qml")
    view.show()
    sys.exit(app.exec())