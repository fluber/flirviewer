from genericpath import exists
import os
from email.mime import application
from fileinput import filename
from typing import Optional

from PySide6.QtCore import QObject, QStandardPaths, QTranslator, Slot
from PySide6.QtWidgets import QFileDialog, QApplication
from PySide6.QtGui import QImage
import logging
import configparser

class FlirManager(QObject):
    def __init__(self, parent: Optional[QObject] = None)-> None:
        super().__init__(parent)
        self.m_config = configparser.ConfigParser()
        ini_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "flir.ini")
        self.m_config.read(ini_file_path)

    @Slot(result=str)
    def pickImage(self):
        fileName = QFileDialog.getOpenFileName(caption=self.tr("Open Raw File"), dir=QStandardPaths.standardLocations(QStandardPaths.PicturesLocation)[0], filter=self.tr("Raw Files (*.raw)"))[0]
        return fileName

    
    @Slot(result=str)
    def pickFlirRawFilePath(self):
        dir = QFileDialog.getExistingDirectory(caption=self.tr("Open Directory"),
                                                dir=str(self.m_config['Main']['flir_raw_file_path']),
                                                options=QFileDialog.Option.ShowDirsOnly
                                                | QFileDialog.Option.DontResolveSymlinks)
        return dir 

    @Slot(result=str)
    def getFlirRawFilePath(self):
        return self.m_config['Main']['flir_raw_file_path']

    @Slot(str, result=str)
    def setFlirRawFilePath(self, path):
        if os.path.exists(path):
            self.m_config['Main']['flir_raw_file_path'] = path
            self.save_ini()

    @Slot(result=str)
    def getStartDate(self):
        return self.m_config['Main']['start_date']

    @Slot(str, result=str)
    def setStartDate(self, path):
        if os.path.exists(path):
            self.m_config['Main']['start_date'] = path
            self.save_ini()
        
    @Slot(result=str)
    def getEndDate(self):
        return self.m_config['Main']['end_date']

    @Slot(str, result=str)
    def setEndDate(self, path):
        if os.path.exists(path):
            self.m_config['Main']['end_date'] = path
            self.save_ini()

    def save_ini(self):
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "flir.ini"),'w') as configfile:
            self.m_config.write(configfile) 

    @Slot()
    def exit(self):
        QApplication.instance().quit()